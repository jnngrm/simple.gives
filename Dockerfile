from node:carbon
workdir /usr/src/app
copy package*.json ./
run yarn install
copy . ./
expose 3000
cmd ["npm", "start"]