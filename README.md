# simple.gives

Always a work in progress, but this is my test project to try out some new Node.JS and AWS tools/services along with using a Stripe white labeled solution.

## Demo

https://www.simplegives.com

Signup and click the activation link.  All onboarding actions, transactions, and recurring donations are done on Stripe's sandbox, i.e. not real.

Or, if you just want to try the donation process:

https://www.simplegives.com/donate?accountId=acct_1FoDF8DEMdoHqsq8

## Quick Start

**Get it**

```bash
git clone git@github.com:jnngrm/simple.gives.git
```

**Yarn it**

```bash
yarn install
```

**Config it**

1. Copy config.example.js to config.js
2. Edit the config.js file and replace [your_email]@gmail.com with your gmail address
3. Replace [your_password] with your gmail password
4. Go here: https://myaccount.google.com/lesssecureapps and toggle less secure apps mode
5. Turn off MFA on your gmail account if you are using it
6. Register a new Stripe account here: https://dashboard.stripe.com/register
7. Follow the instructions and then add your clientId, secret, and key to the stripe section in config.js

**Run it**

The easy way:

```bash
yarn start
```

The slightly more complicated way, but in a nice docker container:

```bash
yarn docker-build
yarn docker-run
```

The Amazon way, mileage will vary:

```bash
AWS_PROFILE={YOUR_PROFILE} ./node_modules/claudia/bin/cmd.js create --timeout 10 --policies ./policies
```

## Anatomy

This is your basic Node.JS Express.JS application.

**Persistence**

For test:

I found a local db for Node.JS that I wanted to try out Nedb:

https://github.com/louischatriot/nedb

Nedb is written to a Mongo interface and is surprisingly simple to bootstrap and work with.

For production:

https://aws.amazon.com/dynamodb/

I have used DynamoDB off and on ancilarily but wanted to dig in and get a more direct integration.  I wrote a simple Mongo wrapper around it, probably not useful for anything beyond this learning project.

**Security**

I initially setup user/pwd creds with password stored using Argon2, however I found that AWS Lambda had some issues pulling in Argon2 dependencies so I switched over to Bcrypt.  I believe Argon2 makes use of a C++ native module.  My best guess is AWS has this particular functionality locked down.

I also used JWT for storing the identity client side securely.  The security middleware will pick up the JWT token from x-access-token header and verify it if it exists and has not expired.

**Communication**

I wanted to try out Nodemailer since it seems to be the defacto standard lib for delivering email.  Looks like it is well supported, allows multipart text and html parts.  See email.js for general usage.

https://nodemailer.com/about/

For test:

It's fairly straightforward to send email through a gmail personal account, however you do have to turn off MFA:

https://developers.google.com/gmail/api/guides/sending

For production:

Also, I wanted to dive back into SES since I had tried it back in 2012 and it was missing some key support for setting up mail domains, DKIM and SPF, and was fairly limited in test.  While still limited to white listed email for test, it's come a long way.  Much easier to setup:

https://aws.amazon.com/ses/

**View**

I'll admit, I really wanted to get started on React here, but unfortunately I needed to prioritize account onboarding for Stripe, so I dropped down into campatibility mode :-/.  I went for pure template drive view via mustache.

https://mustache.github.io/

I will give myself some credit and say I used this as an opportunity to try out Google's Material design:

https://material.io/

via

https://materializecss.com/

**Deployment**

For test:

You can either yarn start that bad boy, or use docker:

```bash
docker build -t simple.gives .
docker run -p 3000:3000 simple.gives
```

Alternately, you can use the built in yarn scripts:

```bash
yarn docker-build
yarn docker-start
yarn docker-stop
```

Sidepanel:
> If you are using SES or Dynamo (or any AWS stuff mentioned here), you will need to set your AWS env vars.
> Access, secret, and region vars are required, see:
> https://docs.aws.amazon.com/cli/latest/userguide/cli-environment.html

For production:

I've used serverless here before, and it does seem to be a good if not a bit broken solution, however poking around a bit I found Claudia:

https://claudiajs.com/

It's good, not great but good.  You can find policy files under policies directory.  It has the oh so fun proxy in place for API Gateway, but it does work and is slightly less brittle than serverless.  Also, as much as I love YAML, config is JSON :-).

To create your lambda:
```bash
AWS_PROFILE={YOUR_PROFILE} ./node_modules/claudia/bin/cmd.js create --timeout 10 --policies ./policies
```

To update:
```bash
AWS_PROFILE={YOUR_PROFILE} ./node_modules/claudia/bin/cmd.js update
```
To destroy:
```bash
AWS_PROFILE={YOUR_PROFILE} ./node_modules/claudia/bin/cmd.js destroy
```
