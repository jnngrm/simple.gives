const aws = require('aws-sdk');

module.exports = {
    net: {
        baseUrl: 'http://localhost:3000',
    },
    stripe: {
        connectAuthorizeUri: 'https://connect.stripe.com/express/oauth/authorize',
        connectTokenUri: 'https://connect.stripe.com/oauth/token',
        clientId: '',
        secret: '',
        key: '',
    },
    auth: {
        secret: 'password',
    },
    mail: {
        transporter: 'gmail',
        gmail: {
            service: 'gmail',
            auth: {
                user: '[your_email]@gmail.com',
                pass: '[your_password]',
            },
        },
        ses: {
            SES: new aws.SES(),
        },
        from: '[your_email]@gmail.com',
    },
    db: {
        type: 'nedb',
    },
};
