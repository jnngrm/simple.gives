const config = require('../config');
const documentClient = new (require('aws-sdk').DynamoDB.DocumentClient);
const Datastore = require('nedb-promise');

/**
 * Get expression attribute values.
 *
 * @param {Object} object the object
 *
 * @return {Object} expression attribute values
 */
function expressionAttributeValues(object) {
    let values = {};
    Object.keys(object).forEach(function(key) {
        values[`:${key}`] = object[key];
    });
    return values;
}

/**
 * Get the query expression.
 *
 * @param {Object} object object
 *
 * @return {String} the expression
 */
function keyConditionExpression(object) {
    let expression;
    Object.keys(object).forEach(function(key) {
        if (expression) {
            expression += ` and ${key} = :${key}`;
        } else {
            expression = `${key} = :${key}`;
        }
    });
    return expression;
}

/**
 * Get the update expression.
 *
 * @param {Object} object object
 *
 * @return {String} the expression
 */
function updateExpression(object) {
    let expression;
    Object.keys(object).forEach(function(key) {
        if (expression) {
            expression += `, ${key} = :${key}`;
        } else {
            expression = `set ${key} = :${key}`;
        }
    });
    return expression;
}

/**
 * Mongo like wrapper for DynamoDB
 *
 * @param {String} name name of the object
 *
 * @return {Object} the wrapper
 */
function dynamoWrapper(name) {
    return {
        insert: (object) => {
            return documentClient.put({
                TableName: name,
                Item: object,
            }).promise();
        },
        find: async (object) => {
            return (await documentClient.query({
                TableName: name,
                ExpressionAttributeValues: expressionAttributeValues(object),
                KeyConditionExpression: keyConditionExpression(object),
            }).promise()).Items;
        },
        findOne: async (object, index) => {
            return (await documentClient.query({
                TableName: name,
                IndexName: index,
                ExpressionAttributeValues: expressionAttributeValues(object),
                KeyConditionExpression: keyConditionExpression(object),
            }).promise()).Items[0];
        },
        update: async (keys, object) => {
            return documentClient.update({
                TableName: name,
                Key: keys,
                ExpressionAttributeValues:
                    expressionAttributeValues(object.$set),
                UpdateExpression: updateExpression(object.$set),
            }).promise();
        },
    };
}

/**
 * Get the database object.
 *
 * @return {Object} the db object
 */
function getDb() {
    if (config.db.type === 'nedb') {
        return {
            user: new Datastore({
                filename: './store/user',
                autoload: true,
                inMemoryOnly: false,
            }),
        };
    } else if (config.db.type === 'dynamodb') {
        return {
            user: dynamoWrapper('user'),
        };
    }
    return null;
}

module.exports = getDb();
