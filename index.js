const express = require('express');
const app = express();
const mustacheExpress = require('mustache-express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const validate = require('express-validation');
const fileUpload = require('express-fileupload');

app.use(fileUpload());
app.use(bodyParser.urlencoded({
    extended: false,
}));
app.use(bodyParser.json());
app.use(cookieParser());
app.engine('mustache', mustacheExpress());
app.set('view engine', 'mustache');
app.set('views', `${__dirname}/views`);
app.use(express.static('public'));

app.use('/', require('./router/main'));
app.use('/donate', require('./router/donate'));
app.use('/login', require('./router/login'));
app.use('/signup', require('./router/signup'));
app.use(require('./security'));
app.use('/dashboard', require('./router/dashboard'));
app.use(function(err, req, res, next) {
    console.error(err);
    if (err instanceof validate.ValidationError) {
        res.status(400).json(err);
    } else {
        res.status(500).send(err);
    }
});

module.exports = app;

