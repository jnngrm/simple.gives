const config = require('./config');
const nodemailer = require('nodemailer');
const mustache = require('mustache');
const fs = require('fs');
const transporter = nodemailer.createTransport(
    config.mail[config.mail.transporter]);

/**
 * Get the template definition
 *
 * @param {String} template the template
 *
 * @return {Object} the template definition
 */
function def(template) {
    return {
        html: {
            filename: `./templates/${template}.mustache`,
        },
        text: {
            filename: `./templates/${template}-text.mustache`,
        },
        subject: {
            filename: `./templates/${template}-subject.mustache`,
        },
    };
}

/**
 * Get the rendered email parts.
 *
 * @param {String} template the template to render
 * @param {Object} context the data context to use for rendering
 *
 * @return {Object} the rendered email parts
 */
function get(template, context) {
    let parts = def(template);
    Object.keys(parts).map(function(part) {
        let content = fs.readFileSync(parts[part].filename).toString('utf-8');
        parts[part].content = mustache.render(content, context);
    });
    return parts;
}

module.exports = {
    send: function(sender, recipient, template, context) {
        let parts = get(template, context);
        return transporter.sendMail({
            from: sender,
            to: recipient,
            subject: parts.subject.content,
            html: parts.html.content,
            text: parts.text.content,
        });
    },
};
