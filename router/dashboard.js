const express = require('express');
const router = express.Router(); //eslint-disable-line
const moment = require('moment');
const config = require('../config');
const stripe = require('stripe')(config.stripe.secret);
const Promise = require('bluebird');
const asyncHandler = require('express-async-handler');
const url = require('url');

/**
 * Count number of charges in period.
 *
 * @param {String} accountId Stripe account ID
 * @param {String} period the period of time to look back
 * @param {Object} context the object to set count on
 *
 * @return {Object} summary
 */
async function summarize(accountId, period) {
  let periodName = {day: 'Daily', week: 'Weekly',
    month: 'Monthly', year: 'Yearly'};
  let data = (await stripe.charges.list({
    created: {
      gt: moment().subtract(1, period).unix(),
    },
    limit: 100,
  }, {
    stripe_account: accountId,
  })).data;
  let sum = data.reduce( (sum, entry) => sum + entry.amount, 0);
 return {
    name: periodName[period],
    total: data.length,
    sum: sum !== 0 ? (sum / 100).toFixed(2) : sum.toFixed(2),
  };
}

router.get('/', asyncHandler(async function(req, res) {
  let account = await stripe.balance.retrieve({
    stripe_account: res.locals.identity.accountId,
  });
  let payouts = (await stripe.payouts.list({
    limit: 100,
  }, {
    stripe_account: res.locals.identity.accountId,
  })).data;
  let summaries = await Promise.map(['day', 'week', 'month', 'year'],
    (period) => summarize(res.locals.identity.accountId, period));
  let withdrawn = payouts.reduce( (sum, entry) => sum + entry.amount, 0);
  res.render('dashboard', {
    pending: (account.pending[0].amount !== 0 ?
      account.pending[0].amount / 100 :
      account.pending[0].amount).toFixed(2),
    available: (account.available[0].amount !== 0 ?
      account.available[0].amount / 100 :
      account.available[0].amount).toFixed(2),
    withdrawn: (withdrawn !== 0 ? withdrawn / 100 : withdrawn).toFixed(2),
    summaries,
  });
}));

router.get('/donation', asyncHandler(async function(req, res) {
  let charge = await stripe.charges.retrieve(req.query.chargeId, {
    stripe_account: res.locals.identity.accountId,
  });
  let customer = await stripe.customers.retrieve(charge.customer, {
    stripe_account: res.locals.identity.accountId,
  });
  charge.formattedCreatedDate =
    moment(charge.created * 1000).format('M-DD-YY h:mm A');
  charge.formattedAmount = `$${charge.amount / 100}`;
  charge.paymentMethod = customer.sources.data[0].object === 'card' ?
    `${customer.sources.data[0].brand} (${customer.sources.data[0].last4})` :
    `${customer.sources.data[0].bank_name} (${customer.sources.data[0].last4})`;
  res.render('donation', {
    charge,
    customer,
    expiration: customer.sources.data[0].object === 'card' ? 'show' : 'hide',
    refund: charge.refunded ? 'hide' : 'show',
  });
}));

router.get('/recurring', asyncHandler(async function(req, res) {
  let subscription =
    await stripe.subscriptions.retrieve(req.query.subscriptionId, {
      stripe_account: res.locals.identity.accountId,
    });
  let customer = await stripe.customers.retrieve(subscription.customer, {
    stripe_account: res.locals.identity.accountId,
  });
  let billingCycleAnchor = moment(subscription.billing_cycle_anchor * 1000);
  subscription.formattedCreatedDate =
    moment(subscription.created * 1000).format('M-DD-YY h:mm A');
  subscription.plan.formattedAmount = `$${subscription.plan.amount / 100}`;
  subscription.billingCycleAnchorFormatted =
    billingCycleAnchor.format('M-DD-YY h:mm A');
  subscription.dayOfMonth = billingCycleAnchor.date();
  subscription.paymentMethod = customer.sources.data[0].object === 'card' ?
  `${customer.sources.data[0].brand} (${customer.sources.data[0].last4})` :
  `${customer.sources.data[0].bank_name} (${customer.sources.data[0].last4})`;
  res.render('recurring', {
    subscription,
    customer,
    expiration: customer.sources.data[0].object === 'card' ? 'show' : 'hide',
    cancel: subscription.status === 'active' ? 'show' : 'hide',
  });
}));

router.get('/donations', asyncHandler(async function(req, res) {
  let chargeList = (await stripe.charges.list({
    limit: 100,
  }, {
    stripe_account: res.locals.identity.accountId,
  })).data;
  await Promise.map(chargeList, async (charge) => {
    charge.customer = await stripe.customers.retrieve(charge.customer, {
      stripe_account: res.locals.identity.accountId,
    });
  });
  let charges = chargeList.map(function(charge) {
    charge.formattedCreatedDate =
      moment(charge.created * 1000).format('M-DD-YY h:mm A');
    charge.formattedAmount = `$${charge.amount / 100}`;
    let refunded = Boolean(charge.refunded);
    charge.statusClass = refunded ? 'red-text' : 'green-text';
    charge.statusContent = refunded ? 'refunded' : 'successful';
    return charge;
  });
  res.render('donations', {
    charges,
  });
}));

router.get('/recurrings', asyncHandler(async function(req, res) {
  let subscriptionList = (await stripe.subscriptions.list({
    status: 'all',
    limit: 100,
  }, {
    stripe_account: res.locals.identity.accountId,
  })).data;
  await Promise.map(subscriptionList, async (subscription) => {
    subscription.customer = await stripe.customers
      .retrieve(subscription.customer, {
        stripe_account: res.locals.identity.accountId,
    });
  });
  let subscriptions = subscriptionList.map(function(subscription) {
    subscription.formattedCreatedDate =
      moment(subscription.created * 1000).format('M-DD-YY h:mm A');
    subscription.plan.formattedAmount =
      `$${subscription.plan.amount / 100}`;
    subscription.statusClass =
      subscription.status === 'active' ? 'green-text' : 'red-text';
    return subscription;
  });
  res.render('recurrings', {
    subscriptions,
  });
}));

router.get('/settings', asyncHandler(async function(req, res) {
  let account = await stripe.accounts.retrieve(res.locals.identity.accountId);
  let logo;
  if (account.business_logo) {
    logo = await stripe.fileUploads.retrieve(account.business_logo);
  }
  res.render('settings', {
    account,
    logo,
    key: config.stripe.key,
    info: req.query.info,
  });
}));

router.post('/settings', asyncHandler(async function(req, res) {
  let logo;
  if (req.files.logo) {
    logo = await stripe.fileUploads.create({
      file: {
        data: req.files.logo.data,
        name: req.files.logo.name,
        type: 'application.octet-stream',
      },
      purpose: 'business_logo',
    });
  }
  await stripe.accounts.update(res.locals.identity.accountId, {
    business_profile: {
        name: req.body.organizationName,
        url: req.body.url,
    },
    default_currency: 'usd',
    account_token: req.body.legalToken,
    external_account: req.body.bankToken ? req.body.bankToken : undefined,
    settings: {
        branding: {
            logo: logo ? logo.id : undefined,
        },
        payments: {
            statement_descriptor: 'simple_gives',
        }
    }
  });
  res.redirect(url.format({
    pathname: '/dashboard/settings',
    query: {
      info: 'Settings saved successfully',
    },
  }));
}));

router.get('/refund', asyncHandler(async function(req, res) {
  await stripe.refunds.create({
    charge: req.query.chargeId,
  }, {
    stripe_account: res.locals.identity.accountId,
  });
  res.redirect(`/dashboard/donation?chargeId=${req.query.chargeId}`);
}));

router.get('/cancel', asyncHandler(async function(req, res) {
  await stripe.subscriptions.del(req.query.subscriptionId, {
    stripe_account: res.locals.identity.accountId,
  });
  res.redirect(`/dashboard/recurring?subscriptionId=${req.query.subscriptionId}`); // eslint-disable-line
}));

router.get('/fundraise', asyncHandler(async function(req, res) {
  let account = await stripe.accounts.retrieve(res.locals.identity.accountId);
  res.render('fundraise', {
    account,
    baseUrl: config.net.baseUrl,
    info: req.query.info,
  });
}));

router.post('/fundraise', asyncHandler(async function(req, res) {
  let account = await stripe.accounts.retrieve(res.locals.identity.accountId);
  let metadata = account.metadata;
  metadata.impact = req.body.impact;
  metadata.coverFee = req.body.coverFee;
  await stripe.accounts.update(res.locals.identity.accountId, {
    metadata,
  });
  res.redirect(url.format({
    pathname: '/dashboard/fundraise',
    query: {
      info: 'Settings saved successfully',
    },
  }));
}));

module.exports = router;
