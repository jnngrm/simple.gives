const express = require('express');
const config = require('../config');
const router = express.Router(); //eslint-disable-line
const stripe = require('stripe')(config.stripe.secret);
const mail = require('../mail');
const validate = require('express-validation');
const asyncHandler = require('express-async-handler');
const db = require('../db');
const plaid = require('plaid');
const plaidClient = new plaid.Client(config.plaid.clientId, config.plaid.secret,
    config.plaid.key, plaid.environments.sandbox);

router.post('/plaid', asyncHandler(async function(req, res) {
  let accessTokenResult = await plaidClient.
    exchangePublicToken(req.body.publicToken);
  res.json(await plaidClient.
    createStripeToken(accessTokenResult.access_token, req.body.accountId));
}));

router.get('/', validate(require('../validation/donate')),
  asyncHandler(async function(req, res) {
    let account = await stripe.accounts.retrieve(req.query.accountId);
    let logo;
    if (account.business_logo) {
      logo = await stripe.fileUploads.retrieve(account.business_logo);
    }
    res.render('donate', {
      account,
      logo,
    });
  }));

router.get('/charge', asyncHandler(async function(req, res) {
  let account = await stripe.accounts.retrieve(req.query.accountId);
  let logo;
  if (account.business_logo) {
    logo = await stripe.fileUploads.retrieve(account.business_logo);
  }
  let coverFee = account.metadata.coverFee;
  let amountFeesCovered = req.query.amount;
  if (coverFee) {
    let percent = parseFloat(coverFee) / 100;
    amountFeesCovered = (((Number(req.query.amount) *
      100) * percent / 100) + Number(req.query.amount)).toFixed(2);
  }
  res.render('charge', {
    amount: req.query.amount,
    type: req.query.type || 'recurring',
    amountFeesCovered,
    showCoverFee: coverFee ? 'show' : 'hide',
    account: account,
    stripeKey: config.stripe.key,
    plaidKey: config.plaid.key,
    logo,
  });
}));

/**
 * Create a customer object.
 *
 * @param {Object} req the request
 *
 * @return {Object} customer object
 */
async function createCustomer(req) {
  return await stripe.customers.create({
    email: req.body.email,
    source: req.body.stripeToken,
    shipping: {
      name: `${req.body.firstName} ${req.body.lastName}`,
      address: {
        city: req.body.city,
        country: 'us',
        line1: req.body.address1,
        line2: req.body.address2,
        postal_code: req.body.zip,
        state: req.body.state,
      },
    },
    metadata: {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
    },
  }, {
    stripe_account: req.body.accountId,
  });
}

/**
 * Get metadata from the request.
 *
 * @param {Object} req the request
 *
 * @return {Object} the metadata object
 */
function getMetadata(req) {
  return {
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
  };
}

/**
 * Send an email receipt for the charge.
 *
 * @param {Object} account the account
 * @param {Object} customer the customer
 * @param {String} id object id for receipt
 * @param {Double} amount the donation amount
 */
async function sendReceipt(account, customer, id, amount) {
  await mail.send(`${account.business_profile.name} <${config.mail.noreply}>`,
    customer.email, 'receipt', {
    id,
    customer,
    account,
    amount,
  });
};

router.post('/recurring', asyncHandler(async function(req, res) {
  let amount = Number((parseFloat(req.body.amount) * 100).toFixed(0));
  let account = await stripe.accounts.retrieve(req.body.accountId);
  let identity = await db.user.findOne({
    accountId: account.id,
  }, 'accountId-index');
  let customer = await createCustomer(req);
  let metadata = getMetadata(req);
  let plan = await stripe.plans.create({
    amount,
    interval: 'month',
    product: {
      name: account.business_profile.name,
    },
    currency: 'usd',
    metadata,
  }, {
    stripe_account: req.body.accountId,
  });
  let subscription = await stripe.subscriptions.create({
    customer: customer.id,
    items: [{
      plan: plan.id,
    }],
    application_fee_percent: Number(identity.applicationFee),
    metadata,
  }, {
    stripe_account: req.body.accountId,
  });
  await sendReceipt(account, customer, subscription.id, req.body.amount);
  res.redirect(`/donate/thankyou?type=recurring&id=${subscription.id}&accountId=${req.body.accountId}`); // eslint-disable-line
}));

router.post('/charge', asyncHandler(async function(req, res) {
  let amount = Number((parseFloat(req.body.amount) * 100).toFixed(0));
  let account = await stripe.accounts.retrieve(req.body.accountId);
  let identity = await db.user.findOne({
    accountId: account.id,
  }, 'accountId-index');
  let customer = await createCustomer(req);
  let metadata = getMetadata(req);
  let charge = await stripe.charges.create({
    amount,
    currency: 'usd',
    customer: customer.id,
    application_fee: Math.trunc(amount *
      (Number(identity.applicationFee) / 100)),
    metadata,
  }, {
    stripe_account: req.body.accountId,
  });
  await sendReceipt(account, customer, charge.id, req.body.amount);
  res.redirect(`/donate/thankyou?type=charge&id=${charge.id}&accountId=${req.body.accountId}`); // eslint-disable-line
}));

router.get('/thankyou', asyncHandler(async function(req, res) {
  let object;
  if (req.query.type === 'charge') {
    object = await stripe.charges.retrieve(req.query.id, {
      stripe_account: req.query.accountId,
    });
  } else if (req.query.type === 'recurring') {
    object = await stripe.subscriptions.retrieve(req.query.id, {
      stripe_account: req.query.accountId,
    });
  }
  let customer = await stripe.customers.retrieve(object.customer, {
    stripe_account: req.query.accountId,
  });
  let account = await stripe.accounts.retrieve(req.query.accountId);
  res.render('thankyou', {
    id: object.id,
    customer,
    account,
    amount: (object.amount || object.plan.amount) / 100,
  });
}));

module.exports = router;
