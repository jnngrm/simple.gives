const express = require('express');
const router = express.Router(); //eslint-disable-line
const jwt = require('jsonwebtoken');
const db = require('../db');
const config = require('../config');
const stripe = require('stripe')(config.stripe.secret);
const asyncHandler = require('express-async-handler');

router.get('/', function(req, res) {
    res.render('login', {
        redirect: req.query.redirect,
    });
});

router.post('/', asyncHandler(async function(req, res) {
    let userList = await db.user.find({
        email: req.body.email,
    });
    if (userList.length !== 0 && userList[0].accountStatus === 'verified' &&
        req.body.password ===userList[0].password) {
        let account = await stripe.accounts.retrieve(userList[0].accountId);
        let logo;
        if (account.business_logo) {
            logo = await stripe.fileUploads.retrieve(account.business_logo);
        }
        userList[0].logo = logo;
        res.cookie('x-access-token', jwt.sign(userList[0], config.auth.secret, {
            expiresIn: '1d',
        }), {
            maxAge: 86400000,
        });
        res.redirect(decodeURIComponent(req.body.redirect) || '/');
    } else {
        res.render('login', {
            redirect: req.body.redirect,
            error: 'Invalid username and/or password.',
        });
    }
}));

module.exports = router;
