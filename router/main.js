const express = require('express');
const router = express.Router(); //eslint-disable-line

router.get('/', function(req, res) {
    res.render('main');
});

module.exports = router;
