const express = require("express");
const config = require("../config");
const stripe = require("stripe")(config.stripe.secret);
const router = express.Router(); //eslint-disable-line
const uuid = require("node-uuid");
const db = require("../db");
const mail = require("../mail");
const asyncHandler = require("express-async-handler");
const querystring = require("querystring");

router.get("/", function(req, res) {
  res.render("signup", {
    stripeConnectUri: config.stripe.connectAuthorizeUri,
    stripeClientId: config.stripe.clientId
  });
});

router.get("/complete", function(req, res) {
  res.render("signupcomplete");
});

router.get("/verifycomplete", function(req, res) {
  res.render("verifycomplete");
});

router.get(
  "/verify",
  asyncHandler(async function(req, res) {
    let userList = await db.user.find({
      email: req.query.email
    });
    if (userList.length !== 0 && userList[0].accountStatus === "unverified") {
      res.render("verify", userList[0]);
    } else {
      res.redirect("/login");
    }
  })
);

router.post(
  "/verify",
  asyncHandler(async function(req, res) {
    let userList = await db.user.find({
      email: req.body.email
    });
    if (
      userList.length > 0 &&
      req.body.verificationCode === userList[0].verificationCode
    ) {
      await db.user.update(
        {
          email: req.body.email
        },
        {
          $set: {
            password: req.body.password,
            accountStatus: "verified"
          }
        },
        {}
      );
    }
    res.redirect("/signup/verifycomplete");
  })
);

router.post(
  "/",
  asyncHandler(async function(req, res) {
    console.log(req.context);
    let userList = await db.user.find({
      email: req.body.email
    });
    if (userList.length === 0) {
      let account = await stripe.accounts.create({
        type: "custom",
        country: "US",
        business_type: "company",
        email: req.body.email,
        company: {
          name: req.body.organizationName,
        },
        business_profile: {
          url: `http://www.${req.body.organizationName.replace(/\W/, '')}.com`
        },
        settings: {
          payments: {
            statement_descriptor: req.body.organizationName.substring(0, 22)
          }
        },
        tos_acceptance: {
          date: Math.floor(Date.now() / 1000),
          ip: "127.0.0.1",
          user_agent: 'chrome'
        },
        requested_capabilities: ["card_payments", "transfers"]
      });
      let verificationCode = uuid.v4();
      await db.user.insert({
        accountId: account.id,
        email: req.body.email,
        organizationName: req.body.organizationName,
        verificationCode,
        applicationFee: 3,
        accountStatus: "unverified"
      });
      await mail.send(config.mail.from, req.body.email, "signup", {
        baseUrl: config.net.baseUrl,
        qs: querystring.stringify({
          email: req.body.email,
          verificationCode
        }),
        helpEmail: config.mail.from
      });
      res.redirect("/signup/complete");
    } else {
      res.render("signup", {
        stripeConnectUri: config.stripe.connectAuthorizeUri,
        stripeClientId: config.stripe.clientId,
        error: "User already registered"
      });
    }
  })
);

module.exports = router;
