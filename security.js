const express = require('express');
const router = new express.Router();
const config = require('./config');
const jwt = require('jsonwebtoken');

router.use(function(req, res, next) {
    let token = req.cookies['x-access-token'];
    if (!res.headersSent && token && jwt.verify(token, config.auth.secret)) {
        res.locals.identity = jwt.decode(token);
        next();
    } else {
        res.redirect(`/login?redirect=${encodeURIComponent(req.originalUrl)}`);
    }
});

module.exports = router;
