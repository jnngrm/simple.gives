const Joi = require('joi');
module.exports = {
    query: {
        accountId: Joi.string().required(),
        type: Joi.string().valid(['recurring', ['charge']]),
    },
};
