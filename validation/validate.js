const Joi = require('joi');
module.exports = (schema) =>
    function(req, res, next, ...args) {
        req.errors = Joi.validate(req, schema);
        next();
    };

